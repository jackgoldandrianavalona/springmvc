/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateDao;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.Article;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Fanjava
 */
@Controller
public class ArticleController {

    @Autowired
    HibernateDao dao;

    @RequestMapping(value = "/insertionproduit")
    public ModelAndView insertion(HttpServletRequest req) {
            ModelAndView mv = new ModelAndView();
            String type = req.getParameter("type");
             String titre = req.getParameter("titre");
            String description = req.getParameter("description");
            Date datedebut = Date.valueOf(req.getParameter("datedebut"));
            Date datefin = Date.valueOf(req.getParameter("datefin"));
      
                Article t = new Article();
                t.setType(type);
                t.setTitre(titre);
                t.setDescription(description);
                t.setDateDebut(datedebut);
                t.setDateFin(datefin);
                 dao.create(t);
                mv.setViewName("ListeArticle");
            return mv;
    }
 @RequestMapping(value = "/getnbpage")
    public ModelAndView getnbpage(HttpServletRequest re) {
        ModelAndView mv = new ModelAndView();
        List list = new ArrayList();
        int lim_par_pages = 2;
       
        if (re.getParameter("page") != null) {
            list = dao.getListe(Integer.parseInt(re.getParameter("page")), lim_par_pages,dao.findAll(Article.class));
        } else {
            list = dao.getListe(1, lim_par_pages,dao.findAll(Article.class));
        }
        mv.setViewName("ListeArticle");
        mv.addObject("listearticle", list);
        return mv;
    }

    @RequestMapping(value = "/ResultatRecherche")
    public ModelAndView search(HttpServletRequest req, HttpServletResponse response) {

        ModelAndView mv = new ModelAndView();
        String search = req.getParameter("type");
       
        List<Article> val = dao.findByNameLike(search);
        mv.setViewName("ResultatRecherche");
        mv.addObject("listearticle",val);
        return mv;
    }

    @RequestMapping(value = "/")
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        return mv;
    }

    @RequestMapping(value = "/formulaireInsertion")
    public ModelAndView formulaireInsertion() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("formulaireInsertion");
        return mv;
    }

    @RequestMapping(value = "/test")
    public ModelAndView pageafa() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("test");
        return mv;
    }

    @RequestMapping(value = "/listearticle")
    public ModelAndView ListeArticle() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("ListeArticle");
        mv.addObject("listeproduit", dao.findAll(Article.class));
        return mv;
    }

    @RequestMapping(value = "/testliste")
    public ModelAndView testliste() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("testliste");
        return mv;
    }

    @RequestMapping(value = "/resultatpagination")
    public ModelAndView resultatpagination(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("ListeArticle");
        Article a = new Article();
        int size = (int) req.getAttribute("page");
        mv.addObject("listeproduit", dao.paginateWhere(a, 3, size));
        return mv;
    }


    public static void main(String[] args) {
        HibernateDao dao = new HibernateDao();
        Type t = new Type();
        t.setIdType(5);
        t.setType("test");
        dao.create(t);

    }

}
