<%-- 
    Document   : listearticle
    Created on : 28 janv. 2023, 17:22:25
    Author     : goldandrianavalona
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="dao.HibernateDao"%>
<%@page import="java.util.List"%>
<%@page import="model.Article"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%  HibernateDao ds = new HibernateDao();
    List<Article> listearticlenbpage = (List) request.getAttribute("listearticle");
    int lim_par_pages = 1;
    int nb_pages = ds.nbpages(lim_par_pages, listearticlenbpage);%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>

    <body>
        <h1>LISTE ARTICLE</h1>

        <form action="ResultatRecherche" method="get">
            <input type="text" name="type">
            <button  type="submit">valider</button>
        </form>
        <%

            for (int i = 0; i < listearticlenbpage.size(); i++) {
                out.print((((Article) listearticlenbpage.get(i)).getType()));
                out.print((((Article) listearticlenbpage.get(i)).getTitre()));
                out.print((((Article) listearticlenbpage.get(i)).getDescription()));
                out.print((((Article) listearticlenbpage.get(i)).getDateDebut()));
                out.print((((Article) listearticlenbpage.get(i)).getDateFin()));
        %></br><%
                      }


        %>
        <nav aria-label="Page navigation">
            <ul class="pagination"> 

                <% for (int i = 1; i <= nb_pages; i++) {%>
                <li><a href="getnbpage?page=<% out.print(i); %>"><% out.print(i); %></a></li>
                    <% }%>
                </li>
            </ul>

        </nav>

    </body>
</html>
