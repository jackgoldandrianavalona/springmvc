<%-- 
    Document   : formulaireInsertion
    Created on : Jan 28, 2023, 11:00:58 PM
    Author     : goldandrianavalona
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>insertion produit</h1>

        <form action="insertionproduit" method="get">
            <select name="type" required>
                <option value="Article">Article</option>
                <option value="Evenement">Evenement</option>
            </select>
            <input type="text" placeholder="titre" name="titre" required>
            <input type="text" placeholder="description" name="description" required>
            <input type="Date" placeholder="datedebut" name="datedebut" required>
            <input type="Date" placeholder="datefin" name="datefin" value="null">
            <button type="submit">valider</button>
        </form>
    </body>
</html>
